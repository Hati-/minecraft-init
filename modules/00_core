#!/bin/bash

check_pidfile_permissions() {
    as_user "touch $(q "$PIDFILE")"
    if ! as_user "test -w $(q "$PIDFILE")" ; then
        echo "Check Permissions. Cannot write to $PIDFILE. Correct the permissions and then excute: $0 status"
        return 1
    fi
    return 0
}

declare -i ROUGE_PIDFILE_PRINTED=0
is_running() {
    # Checks for the minecraft servers screen session
    # returns true if it exists.
    
    if [ -f "$PIDFILE" ]
    then
        read pid < $PIDFILE
        if kill -0 $pid > /dev/null 2>&1
        then
            return 0
        else
            if [ -z "$isInStop" ]
            then
                if [ "$ROUGE_PIDFILE_PRINTED" -eq 0 ]
                then
                    ROUGE_PIDFILE_PRINTED=1
                    echo "Rogue pidfile found! Deleting pidfile."
                    as_user "rm $(q "$PIDFILE")"
                fi
            fi
            return 1
        fi
    else
        pid="$(get_screen_pid)"
        if [ -n "$pid" ]
        then
            echo "No pidfile found, but server's running."
            echo "Re-creating the pidfile."
            
            if check_pidfile_permissions
            then
                as_user "echo $(q "$pid") > $(q "$PIDFILE")"
            fi
            return 0
        else
            return 1
        fi
    fi  
}

get_screen_pid() {
    # Note: the local builtin will overwrite the return value $?, so we need to capture it within local using ret=$?
    local pid=$(as_user "screen -S $(q "$SESSION_NAME") -Q echo -n '\$PID'") ret=$?
    
    if [ "$ret" -ne 0 ]
    then
        # querying the screen session failed, or theres none or several sessions found. resort to fallback:
        pid="$(as_user "screen -list | grep $(q "\.$SESSION_NAME"$'\t') | cut -f1 -d'.' | tr -d -c '0-9\n'")" ret=$?
        
        if [ "$ret" -ne 0 ]
        then
            pid=''
        
            # extra alternative method if needed anytime (might need adjusting)
            # pid=$(ps ax | grep -v grep | grep "${SESSION_NAME} ${INVOCATION}" | cut -f1 -d' ')
        fi
    fi
    
    if [ "$(wc -l <<< $pid)" -gt 1 ]
    then
        echo "Found multiple PIDs matching screen session: $SESSION_NAME; cannot get PID, aborting." >&2
        exit 1
    fi
    
    echo "$pid"
}

declare INVOCATION_WRAPPER
get_invocation_wrapper() {
    INVOCATION_WRAPPER="bash -c $(q "server_wrapper $INVOCATION")"
}

server_wrapper() {
    local RESTART
    
    # Set title of the current screen window (See `man --pager 'less -p ^TITLES' screen`)
    echo -n $'\ek'"$WINDOW_TITLE"$'\e\\'
    
    trap "RESTART=true; echo '$INITSCRIPTNAME: Restart signal received. The server will restart.'" SIGUSR1
    
    # Restart loop
    while true
    do
        RESTART=false
        
        $@
        local ret=$?
        if [ "$ret" -ne 0 ] 
        then
            echo "$INITSCRIPTNAME: Server shut down with exit code $ret. Restarting server...."
        elif [ "$RESTART" = 'true' ]
        then
            echo "$INITSCRIPTNAME: Server shut down with restart signal recieved. Restarting server...."
        else
             # If exit cleanly and restart is not signaled
            echo "$INITSCRIPTNAME: Server shut down with a successful exit code and received no restart signal. The server will not restart automaticly."
            break
        fi
    done
}

declare -a HOOK_MC_START_PRE=()
declare -a HOOK_MC_START_POST=()
mc_start() {
    local HOOK
    local servicejar
    local pid
    local seconds
    
    for HOOK in "${HOOK_MC_START_PRE[@]}"; do $HOOK; done
    
    servicejar=$MCPATH/$SERVICE
    if [ ! -f "$servicejar" ]
    then
        echo "Failed to start: Can't find the specified Minecraft jar under $servicejar. Please check your config!"
        exit 1
    fi

    # Export invocation wrapper specific environment variables
    export -f server_wrapper
    export INITSCRIPTNAME
    export WINDOW_TITLE
    
    get_invocation_wrapper
    as_user "cd $(q "$MCPATH") && screen -dmS $(q "$SESSION_NAME") $INVOCATION_WRAPPER"
    
    if check_pidfile_permissions
    then
        pid="$(get_screen_pid)"
        if [ -n "$pid" ]
        then
            as_user "echo $(q "$pid") > $(q "$PIDFILE")"
        else 
            echo "WARNING: Failed to retrive PID of screen session: $SESSION_NAME; No pidfile will be written!" >&2
            exit 1
        fi
    fi
    
    #
    # Waiting for the server to start
    #
    seconds=0
    until is_running
    do
        sleep 1
        seconds=$seconds+1
        if [[ $seconds -eq 5 ]]
        then
            echo "Still not running, waiting a while longer..."
        fi
        if [[ $seconds -ge 120 ]]
        then
            echo "Failed to start, aborting."
            exit 1
        fi
    done
    echo "$SERVICE is now running."
    
    for HOOK in "${HOOK_MC_START_POST[@]}"; do $HOOK; done
}

mc_signal_restart() {
    if [ -f $PIDFILE ]
    then 
        read pid < $PIDFILE
        get_invocation_wrapper
        xargs -I {} pkill -USR1 -fxP $pid {} <<< $INVOCATION_WRAPPER # send SIGUSR1 to the invocation wrapper
    else 
        echo 'No PIDFile found to signal the server with!'
    fi
}

declare -a HOOK_MC_STOP_PRE=()
declare -a HOOK_MC_STOP_POST=()
mc_stop() {
    local HOOK
    local seconds
    local isInStop
    
    for HOOK in "${HOOK_MC_STOP_PRE[@]}"; do $HOOK; done
    
    #
    # Stops the server
    #
    echo "Saving worlds..."
    mc_command save-all
    sleep 10
    echo "Stopping server..."
    mc_command stop
    sleep 0.5
    #
    # Waiting for the server to shut down
    #
    seconds=0
    isInStop=1
    while is_running
    do
        sleep 1
        seconds=$seconds+1
        if [[ $seconds -eq 5 ]]
        then
            echo "Still not shut down, waiting a while longer..."
        fi
        if [[ $seconds -ge 120 ]]
        then
            local ERRORMSG="Failed to shut down server, aborting."
            echo "$ERRORMSG"
            [ -x "$(command -v logger)" ] && logger -t "$INITSCRIPTNAME" "$ERRORMSG"
            exit 1
        fi
    done
    as_user "rm $(q "$PIDFILE")"
    unset isInStop
    is_running # to detect rouge pidfile
    echo "$SERVICE is now shut down."
    
    for HOOK in "${HOOK_MC_STOP_POST[@]}"; do $HOOK; done
}

mc_command() {
    if is_running
    then
        as_user "screen -S $(q "$SESSION_NAME") -p $(q "$WINDOW_TITLE") -X stuff $(q "${FORMAT//\{\}/$@}"$'\r')"
    else
        echo "$SERVICE was not running. Not able to run command."
    fi
}

mc_say() {
    if is_running
    then
        echo "Said: $@"
        mc_command "say $@"
    else
        echo "$SERVICE was not running. Not able to say anything."
    fi
}

cmd_start() {
    # Starts the server
    if is_running; then
        echo "Server already running."
    else
        mc_start
    fi
}

cmd_stop() {
    # Stops the server
    if is_running; then
        mc_say "SERVER SHUTTING DOWN IN 10 SECONDS!"
        mc_stop
    else
        echo "No running server."
    fi
}

cmd_restart() {
    # Restarts the server
    if is_running; then
        mc_say "SERVER REBOOT IN 10 SECONDS!"
        mc_stop
    else
        echo "No running server, starting it..."
    fi
    mc_start
}

cmd_signal_restart() {
    echo 'Signalling restart.'
    mc_signal_restart
}

cmd_soft_restart() {
    # Restarts the server
    if is_running; then
        mc_say "SERVER REBOOT IN 10 SECONDS!"
        mc_signal_restart
        
        echo "Saving worlds..."
        mc_command save-all
        sleep 10
        echo "Stopping server..."
        mc_command stop
    else
        echo "No running server, starting it..."
        mc_start
    fi
}

cmd_screen() {
    if is_running; then
    as_user "script /dev/null -q -c $(q "screen -xr '$SESSION_NAME' -p '$WINDOW_TITLE'")"
    else
    echo "Server is not running. Do you want to start it?"
    echo "Please put \"Yes\", or \"No\": "
    read START_SERVER
    case "$START_SERVER" in
        [Yy]|[Yy][Ee][Ss])
            mc_start
            as_user "script /dev/null -q -c $(q "screen -xr '$SESSION_NAME' -p '$WINDOW_TITLE'")"
            ;;
        [Nn]|[Nn][Oo])
            clear
            echo "Aborting startup!"
            sleep 1
            clear
            exit 1
            ;;
        *)
            clear
            echo "Invalid input"
            sleep 1
            clear
            exit 1
            ;;
    esac
    fi
}

cmd_command() {
    if is_running; then
        mc_command "$@"
        echo "Sent command: $@"
    else
        echo "No running server to send a command to."
    fi
}

cmd_say() {
    # Says something to the ingame chat
    if is_running; then
        mc_say "$@"
    else
        echo "No running server to say anything."
    fi
}

mod_start=(         'start'          cmd_start          $'\t Starts the server')
mod_stop=(          'stop'           cmd_stop           $'\t Stops the server')
mod_restart=(       'restart'        cmd_restart        $'\t Restarts the server')
mod_signal_restart=('signal-restart' cmd_signal_restart $' Signal the server to automaticly restart after shutdown')
mod_soft_restart=(  'soft-restart'   cmd_soft_restart   $' Restart server but keep server screen. Same as signal-restart then stop.')
mod_screen=(        'screen'         cmd_screen         $'\t Shows the server screen')
mod_command=(       'command'        cmd_command        $'\t Run a command in the server console')
mod_say=(           'say'            cmd_say            $'\t\t Prints the given string to the ingame chat')

EXPORT_MODULES+=(mod_start mod_stop mod_restart mod_signal_restart mod_soft_restart mod_screen mod_command mod_say)
