Init script for minecraft/bukkit servers
=======================================
A init script that apart from starting and stopping the server correctly also has some extra features
for running a mincraft/craftbukkit server.

Fork of https://github.com/Ahtenus/minecraft-init

# **NB! THIS README IS INCOMPLETE AND OUTDATED, DON'T TAKE ANYTHING BELOW FOR GRANTED!**
<br>
<br>

Features
--------

 * Utilization of ramdisk for world data, decreases lag when getting world chunks
 * Cleaning of server.log, a big log file slows down the server
 * Backup for worlds
 * Server updating and complete backup
 * Exclude files and directories from full backup by adding them to "exclude.list"

Requirements
------------
screen, rsync

Setup
=====

1. Make a copy of `servers/server.config.example` with a name of your choice suffixed with `.config` in the `servers` folder.
	 For example `servers/myserver1.config` will be used in this setup guide.
	 
2. Make a folder with the same name within the `servers` folder. For example `servers/myserver1`. Put your minecraft server jar and files in that folder.	 

3. Create a script file with name of your choice. For example `minercraft-myserver1`. Inside it put this code:

		#!/bin/bash
		$(dirname $(readlink -e $0))/minecraft myserver1 "$@"

4. Symlink the the newly created `minercraft-myserver1` file to `/etc/init.d/minecraft`, set the required premissions and update rc.d as shown:

		sudo ln -s ~/minecraft-init/minercraft-myserver1 /etc/init.d/minercraft-myserver1
		chmod 755  ~/minecraft-init/minercraft-myserver1
		sudo update-rc.d minercraft-myserver1 defaults

5. Edit the variables in `main.config` and `servers/myserver1.config` to your liking.

6. Move your worlds to the folder specified by `$WORLDSTORAGE`

7. To load a world from ramdisk run:

		/etc/init.d/minercraft-myserver1 ramdisk WORLDNAME
	
	to disable ramdisk, run the same command again.

---

For more help with the script, run

	/etc/init.d/minercraft-myserver1 help
	
or simply

	./minercraft help
	

Access server console
=====================

	./minercraft SERVERCONFIG screen

Exit the console
	
	Ctrl+A D
