#!/bin/bash
# /etc/init.d/minecraft

### BEGIN INIT INFO
# Provides:   minecraft
# Required-Start: $local_fs $remote_fs
# Required-Stop:  $local_fs $remote_fs
# Should-Start:   $network
# Should-Stop:    $network
# Default-Start:  2 3 4 5
# Default-Stop:   0 1 6
# Short-Description:    Minecraft server
# Description:    Init script for minecraft/bukkit server, with rolling logs and use of ramdisk for less lag.
### END INIT INFO

# Created by Ahtenus

# Based on http://www.minecraftwiki.net/wiki/Server_startup_script
# Support for multiworld by Benni-chan
# Log rolls without needing restarts by Solorvox
# Option for killing server in an emergency by jbondhus

INITSCRIPTNAME='minecraft-init'

get_script_location() {
    echo $(dirname "$(readlink -e "$0")")
}

ME="$(whoami)"
as_user() {
    if [ "$ME" == "$USERNAME" ]
    then
        bash -c "$1"
    else
        su "$USERNAME" -s /bin/bash -c "$1"
    fi
    return $?
}

error() {
    echo -e "$@"
    [ -x "$(command -v logger)" ] && logger -t "$INITSCRIPTNAME" "$1"
    exit 1
}

# Utility function to escape variable strings when using as_user
q() {
	printf '%q' "$*"
}

trim_log() {
	sed 's/^\[.\+\]: //'
}

join_by() {
  local d=${1-} f=${2-}
  if shift 2; then
    printf %s "$f" "${@/#/$d}"
  fi
}

get_module_files() {
    local MODULE
    local I
    local MODULEFILE
    
    for MODULE in "${MODULES[@]}"
    do
        if [ "${MODULE:0:1}" = '-' ]
        then
            MODULE="${MODULE:1}" # Variable should actually be named DISABLEDMODULE
            for I in ${!MODULEFILES[@]}
            do
                MODULEFILE="${MODULEFILES[$I]}"
                if [ "${MODULEFILE##$MODULEFOLDER/[0-9][0-9]_}" = "$MODULE" ]
                then
                    unset -v MODULEFILES[$I]
                    
                    # Alternative method that reindexes the array instead of simply removing the
                    # unwanted module, in case a dense (non-sparse) array is required and necessary 
                    #MODULEFILES=( "${MODULEFILES[@]:0:$I}" "${MODULEFILES[@]:$I+1}" )
                fi
            done
        else 
            IFS=$'\n' MODULEFILES+=( $(find "$MODULEFOLDER" -name "[0-9][0-9]_$MODULE") )
        fi
    done

    # Remove duplicates files but keep ordering
    IFS=$'\n' MODULEFILES=( $(printf '%s\n' "${MODULEFILES[@]}" | nl - | sort -uk2 | sort -nk1 | cut -f2 -) ) 
}

find_and_execute_module() {
    try_module() {
        local MODULEREF
        declare -n MODULES="$1"
        declare -n HELP="$2"
        
        for MODULEREF in "${MODULES[@]}"
        do
            declare -n MODULE="$MODULEREF"
            local COMMAND="${MODULE[0]}"
            if [[ $SHOW_HELP -eq 0 && "${COMMAND%% *}" = "$3" ]]
            then
                shift 3
                ${MODULE[1]} "$@"
                [ $SHOW_HELP -eq 0 ] && exit 0
            fi
            HELP+=("$COMMAND "$'\t'"${MODULE[2]}")
        done
    }
    
    # Try to run global modules
    try_module "EXPORT_GLOBAL_MODULES" "GLOBALHELP" "$@"
    
    # Try to run server modules
    try_module "EXPORT_MODULES" "MODULEHELP" "$@"
}

cmd_global_help() { # Dummy command
    :
}

cmd_global_servers() {
    IFS=$'\n' local servers=( $(find ${SERVERSFOLDER} -maxdepth 1 -name '*.config' -exec basename {} .config \;) )
    echo "Available server configs:"
    echo "$(join_by $'\n' ${servers[@]})"
}

is_cmd_global() {
    local MODULEREF
    local COMMAND
    
    for MODULEREF in "${EXPORT_GLOBAL_MODULES[@]}"
    do
        declare -n MODULE="$MODULEREF"
        COMMAND="${MODULE[0]}"
        if [ "${COMMAND%% *}" = "$1" ]
        then
            return 0
        fi
    done
    return 1
}

verify_config() {    
    if [ ! -d "$MCPATH" ]
    then
        error "ERROR: MCPATH config value ($MCPATH) doesn't point to a valid directory!"
    fi
    if [ ! -e "$MCPATH/$SERVICE" ]
    then
        error "ERROR: SERVICE config value ($SERVICE) doesn't point to a valid file inside MCPATH ($MCPATH)!"
    fi
}

main() {
    # Loads main config file
    local MAINCONFIGFILE="$(get_script_location)/main.config"
    if [ -e "$MAINCONFIGFILE" ]
    then
        source "$MAINCONFIGFILE"        
    else
        error "Couldn't load config file ($MAINCONFIGFILE), file is missing."
    fi
    
    # Resolve folder paths
    local MODULEFOLDER="$(get_script_location)/${MODULEFOLDER}"
    local SERVERSFOLDER="$(get_script_location)/${SERVERSFOLDER}"
    
    # Get module files
    declare -a MODULEFILES=()
    get_module_files
    
    modules_closure() {
        local MODULEFILE
    
        # Setup export variables
        declare -a EXPORT_MODULES=() # Should be renamed to EXPORT_SERVER_MODULES
        declare -a EXPORT_GLOBAL_MODULES=()
        global_help=(   'help'      cmd_global_help     $'\t Show this help')
        global_servers=('servers'   cmd_global_servers  $'\t Prints available servers configs')
        EXPORT_GLOBAL_MODULES+=(global_help global_servers)

        # Import modules
        for MODULEFILE in "${MODULEFILES[@]}"
        do
            source "$MODULEFILE"
        done
        
        # Loads server config file if first argument isn't a global command
        if ! is_cmd_global "$1"
        then
            SERVERCONFIGFILE="${SERVERSFOLDER}/$1.config"
            if [ -e "$SERVERCONFIGFILE" ]
            then
                source "$SERVERCONFIGFILE"
                verify_config
                HAS_SERVERCONFIG=1
            else
                local ERRMSG="Couldn't load server config file for $1, please edit server.config.example and save it as "$SERVERCONFIGFILE"\n"
                ERRMSG+="To see available server configs do \"$(basename $0) servers\""
                error "$ERRMSG"
            fi
            shift
        fi
        
        if [[ "$1" =~ ^(help|--help|-h) ]]
        then 
            SHOW_HELP=1
        fi
        
        # Find and execute module, and fill help in case none matches.
        find_and_execute_module "$@"
    }
    
    local SERVERCONFIGFILE
    local HAS_SERVERCONFIG=0
    local SHOW_HELP=0
    declare -a GLOBALHELP
    declare -a MODULEHELP
    modules_closure "$@"
    
    # Show help
    if [ $SHOW_HELP -ne 0 ]
    then
        echo "Usage: $(basename $0) <SERVERCONFIG> COMMAND"
        echo 'Server commands requires SERVERCONFIG to be specified.'
        echo
        echo 'Available global commands:'
        printf '   %s\n' "${GLOBALHELP[@]}"
        echo
        echo 'Available server commands:'
        printf '   %s\n' "${MODULEHELP[@]}"
        exit 0
    else
        if [ $HAS_SERVERCONFIG -ne 0 ]
        then
            echo "No such command for $(basename "$SERVERCONFIGFILE" .config), see $(basename $0) help"
        else
            echo "No such command, see $(basename $0) help"
        fi
        exit 1
    fi
}

main "$@"